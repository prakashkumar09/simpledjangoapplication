from django.views import generic
from .models import ProductList, ProductDescription
class ProductListView(generic.ListView):
    model=ProductList 
    template_name='p_list.html'
    context_object_name='object_list'

class ProductDetailView(generic.DetailView):
    model=ProductDescription
    template_name='p_details.html'
