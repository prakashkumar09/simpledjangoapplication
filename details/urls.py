from __future__ import absolute_import
from django.conf.urls import patterns,include,url
from details import views



urlpatterns=patterns(
        '',
        url(r'^$',views.ProductListView.as_view(),name='list'),
        url(r'^d/(?P<slug>[-\w]+)/$', views.ProductDetailView.as_view(), name='detail'),
        )
