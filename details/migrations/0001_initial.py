# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProductDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('p_name', models.CharField(max_length=255, blank=True)),
                ('p_cost', models.IntegerField()),
                ('p_description', models.TextField(blank=True)),
                ('slug', models.SlugField(max_length=255, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('p_id', models.CharField(max_length=50, null=True)),
                ('p_name', models.CharField(max_length=255, null=True)),
                ('p_image', models.CharField(max_length=200, null=True)),
                ('slug', models.SlugField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
