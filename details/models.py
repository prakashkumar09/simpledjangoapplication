from django.db import models
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

class ProductList(models.Model):
    p_id = models.CharField(max_length=50,null=True)
    p_name = models.CharField(max_length=255,null=True)
    p_image=models.CharField(max_length=200,null=True)
    slug = models.SlugField(max_length=255)
    def __str__(self):              # __unicode__ on Python 2
        return "%s the productlist" % self.p_name
    def save(self, *args, **kwargs):
        # take model's name and and slugify it
        self.slug = slugify(self.p_name)
        super(ProductList, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})

class ProductDescription(models.Model):
    p_name=models.CharField(max_length=255,blank=True)
    p_cost=models.IntegerField(null=True)
    p_description = models.TextField(blank=True)
    slug=models.SlugField(max_length=255,blank=True)
    

    def __str__(self):              # __unicode__ on Python 2
        return "%s the description " % self.p_name
    def save(self, *args, **kwargs):
        self.slug=slugify(self.p_name)
        super(ProductDescription, self).save(*args,**kwargs)
