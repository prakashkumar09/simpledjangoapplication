from django.conf.urls import patterns, include, url
from django.contrib import admin
from views import HomePageView
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ecommerce.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$',HomePageView.as_view(),name='home'),
    url(r'^details/',include('details.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
