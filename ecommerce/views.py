from __future__ import absolute_import
from django.views import generic


from django.contrib.auth.models import User


from braces import views

from .models import ProductList
#def home_page(request):
#    return 'hello'

class HomePageView(generic.TemplateView):
    model=User
    data=ProductList.objects.all()
    template_name='home.html'
    



